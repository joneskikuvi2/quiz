﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz
{
    public static class Program
    {
    
     public   static long GetFib(long n)
        {
            long previousFib = 0, currentFib = 1, nextFib = 0;

            
            if (n == 0)
                return previousFib;

            for (long i = 2; i <= n; i++)
            {
                nextFib = previousFib + currentFib;
                previousFib =currentFib;
               currentFib = nextFib;
            }

            return currentFib;
        }
        static void Main(string[] args)
        {
           
        }
    }
}
