﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Quiz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz.Tests
{
    [TestClass()]
    public class ProgramTests
    {
        [TestMethod()]
        [DataRow(0, 0)]
        [DataRow(1, 1)]
        [DataRow(2,1)]
        [DataRow(9, 34)]
        [DataRow(50, 12586269025)]
        [DataRow(21, 10946)]
        [DataRow(39, 63245986)]
        [DataRow(58, 591286729879)]
        [DataRow(70, 190392490709135)]



        public void GetFibTest(long testvalue,long fib)
        {
        


            Assert.AreEqual(Program.GetFib(testvalue),fib);
        }
    }
}